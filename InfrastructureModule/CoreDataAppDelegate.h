//
//  CoreDataAppDelegate.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

/**
 *  This extension to the app delegate protocol
 *  allows for easy access to the managed object
 *  context, created and managed by the app delegate.
 */
@protocol CoreDataAppDelegate <UIApplicationDelegate>

/**
 *  The app-wide managed object context.
 */
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

/**
 *  The app-wide managed object model.
 */
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;

/**
 *  The app-wide store.
 */
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

/**
 *  Save the default context.
 */
- (void)saveContext;

@end
