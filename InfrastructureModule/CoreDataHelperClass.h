//
//  CoreDataHelperClass.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 7/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerEntity.h"

/**
 *  Encapsulates methods that help using core data.
 */
@interface CoreDataHelperClass : NSObject

/**
 *  Checks if an object with the given value of 'attributeName'
 *  already exists in the store.
 *
 *  @param entityName Same as expected return type.
 *  @param attributeName Attribute the check.
 *  @param attributeValue The value in question of attributeName.
 *  @param context The context for the search.
 *
 *  @return The existing object, if found. Else nil.
 */
+ (NSManagedObject*) checkForExistingEntity:(NSString*)entityName
                          usingKeyAttribute:(NSString*)attributeName
                                  withValue:(id)attributeValue
                                  inContext:(NSManagedObjectContext*)context;

/**
 *  All the objects from the source array will be either inserted
 *  into the persistent store or updated, if they have existed before.
 *  Whether or not an object already exists is decided by the
 *  primary key defined by attributeName.
 *
 *  Assumes that the primary key can be transformed into an integer value.
 *
 *  @param entityClass The type of the objects that are passed in.
 *  @param dictKey The primary key name in the input dictionaries.
 *  @param attributeName The primary key of the core data entities.
 *  @param objects The objects as dictionaries.
 *  @param context The context to use for the insertion.
 *
 *  @return nil, if no error occurred. Else, the error object.
 */
+ (NSError*) saveOrUpdateAllServerEntitiesOfType:(Class<ServerEntity>)entityClass
                        withPrimaryDictionaryKey:(NSString*)dictKey
                         andPrimaryAttributeName:(NSString*)attributeName
                                      fromSource:(NSArray*)objects
                                            into:(NSManagedObjectContext*)context;

@end
