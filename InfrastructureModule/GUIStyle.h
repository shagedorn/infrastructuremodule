//
//  GUIStyle.h
//  ERP-App
//
//  Created by Sebastian Hagedorn on 13/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  The class applies global style, layout, and colour information
 *  to objects implementing the UIAppearance protocol.
 */
@interface GUIStyle : NSObject

/**
 *  This class implements the singleton pattern. Always
 *  use this class method to get an instance.
 */
+ (GUIStyle*) sharedInstance;

/**
 *  Apply all styles.
 */
- (void) apply;

@end
