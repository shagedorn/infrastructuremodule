//
//  GUIStyle.m
//  ERP-App
//
//  Created by Sebastian Hagedorn on 13/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "GUIStyle.h"

#pragma mark - Private Interface

@interface GUIStyle ()

@property (nonatomic, strong) NSDictionary *plist;

- (id) initSingleton;
- (UIColor*) parseColour:(NSDictionary*)colourInfo;

@end

@implementation GUIStyle

#pragma mark - Public API

static GUIStyle *_instance;

+ (GUIStyle *)sharedInstance{
    if (!_instance) {
        _instance = [[GUIStyle alloc] initSingleton];
    }
    return _instance;
}

- (void)apply {

    // Progess Bar
    NSDictionary *progress = [self.plist objectForKey:@"progressBar"];
    UIColor *progressColour = [self parseColour:[progress objectForKey:@"colour"]];
    NSDictionary *style = [progress objectForKey:@"style"];
    UIProgressViewStyle barStyle = ([[style objectForKey:@"barStyle"] boolValue])? UIProgressViewStyleBar : UIProgressViewStyleDefault;

    // Navigation Bar
    NSDictionary *navBarInfo = [self.plist objectForKey:@"navigationBar"];
    NSDictionary *tintColourInfo = [navBarInfo objectForKey:@"colour"];
    UIColor *tintColour = [self parseColour:tintColourInfo];

    // apply
    [[UIProgressView appearance] setProgressTintColor:progressColour];
    [[UIProgressView appearance] setProgressViewStyle:barStyle];
    [[UINavigationBar appearance] setTintColor:tintColour];
}

#pragma mark - Private API

- (id)initSingleton {
    if (self = [super init]) {

        // Get plist with style information - hold it in memory
        NSString *path = [[NSBundle mainBundle] pathForResource:@"ColourScheme" ofType:@"plist"];
        NSDictionary *plist = [NSDictionary dictionaryWithContentsOfFile:path];
        self.plist = plist;
        
    }
    return self;
}

- (UIColor *)parseColour:(NSDictionary*) colourInfo {
    NSNumber *r = [colourInfo objectForKey:@"r"];
    NSNumber *g = [colourInfo objectForKey:@"g"];
    NSNumber *b = [colourInfo objectForKey:@"b"];
    NSNumber *a = [colourInfo objectForKey:@"alpha"];

    return [UIColor colorWithRed:r.floatValue
                           green:g.floatValue
                            blue:b.floatValue
                           alpha:a.floatValue];
}

@end
