//
//  BundleResourceHelperClass.m
//  InfrastructureModule
//
//  Created by Sebastian Hagedorn on 15/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "BundleResourceHelperClass.h"

@implementation BundleResourceHelperClass

+ (NSString *)pathForResource:(NSString *)resName ofType:(NSString *)ext inModule:(NSString *)moduleName {

    // Get Bundle
    NSBundle *bundle = [BundleResourceHelperClass bundleForModule:moduleName];
    
    // Get resource
    NSString *path = [bundle pathForResource:resName ofType:ext];

    return path;
}

+ (NSBundle *)bundleForModule:(NSString *)moduleName {
    // Get bundle URL
    NSURL *bundleURL = [[NSBundle mainBundle] URLForResource:moduleName
                                               withExtension:@"bundle"];

    // Load bundle
    NSBundle *bundle = [NSBundle bundleWithURL:bundleURL];
    return bundle;
}

@end