//
//  Module.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

/*
 *  Forward declaration as Module protocol is used in typedefs
 */
@protocol Module;

///==========================================
/** Typedefs for blocks used in the protocol */
///==========================================
/**
 *  A block that will be called on data loading updates.
 *
 *  @param progress The (total) progress.
 *  @param sender The module that sends the progress.
 */
typedef void (^UpdateBlock)(NSNumber *progress, id<Module> sender);

/**
 *  A block that will be called on data loading completion.
 *  
 *  @param error A loading error, if any occurred. Pass nil
 *  if no errors occurred.
 *  @param sender The module that has finished loading.
 */
typedef void (^CompletionBlock)(NSError *error, id<Module> sender);

/**
 *  This block is needed within the CloseModuleBlock. It is
 *  called after an animation has finished.
 */
typedef void (^AnimationHasFinishedBlock)();

/**
 *  This block saves the code to close a module. The block is
 *  set by the StartScreenController and abstracts from the way
 *  (modal, navigation, tab,...) the module is presented.
 *
 *  @param block The block gives the module the opportunity to
 *  release resources not until it is off screen.
 */
typedef void (^CloseModuleBlock)(AnimationHasFinishedBlock block);

/**
 *  Every module should implement this protocol in order to be launched
 *  by the launch screen.
 *
 *  It is recommended that the module's base view controller implements
 *  this protocol. Modules need to be added to the 'ActiveModules'-PLIST
 *  in order to be loaded. Please make sure they are linked in the app
 *  as well. As modules are never imported, the '-ObjC' linker flag
 *  must always be set.
 */
@protocol Module <NSObject>

/**
 *  This block saves the code to close a module. The block is
 *  set by the StartScreenController and abstracts from the way
 *  (modal, navigation, tab,...) the module is presented.
 */
@property (nonatomic, copy) CloseModuleBlock closeModuleBlock;

/**
 *  The image will be displayed on the launch screen.
 *
 *  Ideally, it is 200x200px in size (400x400 for retina displays).
 *
 *  @return The launch screen image.
 */
- (UIImage*) modulePreviewImage;

/**
 *  The controller will be displayed when the module is launched.
 *
 *  It is expected to provide UI and functionality to launch
 *  all further view controllers of this module. Usually, this
 *  will be 'self', but it gives you the opportunity to pass
 *  a container view controller.
 *
 *  @return The base view controller.
 */
- (UIViewController*) moduleLaunchController;

/**
 *  When all modules have been loaded, the infrastructure
 *  will check whether each module's dependencies could
 *  also be loaded.
 *
 *  The dependencies are also used to derive the order in
 *  which the modules' data is loaded.
 *
 *  Instead of the full module names, use the ModuleDisplayName
 *  defined in the 'ModuleDescription'-PLISTs. It is used for
 *  matchmaking with the main core data entities.
 *
 *  @return A list of modules.
 */
- (NSArray*) moduleDependencies;

/**
 *  Perform initial or incremental data loading.
 *
 *  The loading should be performed on a separate execution queue.
 *
 *  @param context Loaded entities will be inserted into the database using this context.
 *  @param updateBlock When new data has been
 *  inserted, the updateBlock should be executed.
 *  @param completionBlock When loading data has
 *  finished, the completionBlock should be executed.
 */
- (void) moduleLoadData:(NSManagedObjectContext*)context
            usingBlockForProgress:(UpdateBlock)updateBlock
                    andCompletion:(CompletionBlock)completionBlock;

/**
 *  Return the name of the module.
 *
 *  This name is also used for dependency resolution. The information is
 *  held in a property list inside the module bundle (Key: ModuleDisplayName).
 */
- (NSString*) moduleDescription;

@end
