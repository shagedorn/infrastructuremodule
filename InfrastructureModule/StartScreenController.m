//
//  StartScreenController.m
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "StartScreenController.h"
#import "Module.h"
#import "CoreDataAppDelegate.h"
#import "BundleResourceHelperClass.h"

#pragma mark - Constants

#define MODULE_PREVIEW_WIDTH 200
#define MODULE_PREVIEW_PADDING 100

#pragma mark - Private Interface

@interface StartScreenController () {}

#pragma mark - IB Outlets

@property (weak, nonatomic) IBOutlet UIScrollView *moduleScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *tenantNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *loadAllDataButton;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;

#pragma mark - IB Actions

/**
 *  Starts loading process for modules without dependencies.
 *  Other modules are queued for later data loading.
 *
 *  @param sender The loading button.
 */
- (IBAction)loadAllData:(id)sender;

#pragma mark - Private Interface

/**
 *  All the modules that are active.
 *
 *  All elements of the array should conform to the
 *  Module protocol.
 */
@property (nonatomic, strong) NSMutableArray *loadedModules;

/**
 *  Save each module's loading progress separately.
 */
@property (nonatomic, strong) NSMutableDictionary *moduleLoadingProgress;

/**
 *  Some modules require other modules to load their data first.
 */
@property (nonatomic, strong) NSMutableArray *queuedModules;

/**
 *  Try to load all module classes that are specified in 'ActiveModules.plist'.
 *
 *  After the class was loaded dynamically, it is checked whether it
 *  actually implements the Module protocol.
 */
- (void)loadModules;

/**
 *  This method will log a warning and unload all modules whose
 *  dependencies (other modules) have not been loaded.
 *
 *  This is done recursively.
 */
- (void) checkDependencies;

/**
 *  Displays all loaded modules on the scroll view.
 */
- (void) displayModules;

/**
 *  Start a modole when clicked.
 *
 *  @param sender The button that was touched.
 */
- (void) startModule:(id)sender;

/**
 *  A module has loaded new data.
 *
 *  @param progress The total progress of the module.
 *  @param sender The module that sent the event.
 */
- (void) updateProgress:(NSNumber*)progress fromModule:(id<Module>)sender;

/**
 *  A module has finished loading data.
 *
 *  @param error An error, if any has occurred. Else, nil.
 *  @param sender The module that sent the event.
 */
- (void) moduleHasFinishedLoading:(NSError*)error sender:(id<Module>)sender;

/**
 *  Checks whether depending modules have finished loading.
 *
 *  @param module The module in question.
 *  @return YES, if all dependencies have finished loading.
 */
- (BOOL) moduleMayStartLoadingData:(id<Module>)module;

/**
 *  Start loading a module's data without further checks
 *  for dependencies. Call moduleMayStartLoadingData: first.
 *
 *  @param module The module that is supposed to start loading its data.
 */
- (void) loadModuleData:(id<Module>)module;

@end

#pragma mark - Implementation

@implementation StartScreenController {}

#pragma mark - Lifecycle

- (id)init {
    NSBundle *myBundle = [BundleResourceHelperClass bundleForModule:@"InfrastructureModuleBundle"];
    self = [super initWithNibName:@"StartScreenController" bundle:myBundle];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    // General view adjustments
    self.moduleScrollView.layer.cornerRadius = 5.0f;

    self.logoImageView.image = [UIImage imageNamed:@"Icon-72~ipad.png"];
    self.logoImageView.layer.cornerRadius = 15.0f;
    self.logoImageView.layer.borderWidth = 3;
    self.logoImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;

    self.tenantNameLabel.text = [[NSBundle mainBundle].infoDictionary objectForKey:@"TenantName"];

    // Style button
    NSString *titleString = NSLocalizedString(@"Infr.StartScreenController.LoadAll", @"Button to load all modules' data");
    [self.loadAllDataButton setTitle:titleString forState:UIControlStateNormal];

    NSString *buttonBackgroundPath = [BundleResourceHelperClass pathForResource:@"button"
                                                                         ofType:@"png"
                                                                       inModule:@"InfrastructureModuleBundle"];
    UIImage *resizableImage = [UIImage imageWithContentsOfFile:buttonBackgroundPath];
    resizableImage = [resizableImage resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)
                                                                             resizingMode:UIImageResizingModeStretch];

    buttonBackgroundPath = [BundleResourceHelperClass pathForResource:@"button_pressed"
                                                               ofType:@"png"
                                                             inModule:@"InfrastructureModuleBundle"];
    UIImage *resizablePressedImage = [UIImage imageWithContentsOfFile:buttonBackgroundPath];
    resizablePressedImage = [resizablePressedImage resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)
                                                                                   resizingMode:UIImageResizingModeStretch];
    [self.loadAllDataButton setBackgroundImage:resizableImage forState:UIControlStateNormal];
    [self.loadAllDataButton setBackgroundImage:resizablePressedImage forState:UIControlStateHighlighted];

    // Load modules
    [self loadModules];
    [self displayModules];
}

- (void)loadModules {
    DLog(@"Loading modules");
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"ActiveModules" ofType:@"plist"];
    NSArray *plist = [NSArray arrayWithContentsOfFile:plistPath];
    self.loadedModules = [NSMutableArray arrayWithCapacity:10];

    for (NSDictionary *moduleInfo in plist) {
        NSString *moduleClassName = [moduleInfo objectForKey:@"ModuleClass"];
        Class moduleBaseClass = NSClassFromString(moduleClassName);
        if (moduleBaseClass) {
            DLog(@"Module found: %@", NSStringFromClass(moduleBaseClass));
            id<Module> instance = [[moduleBaseClass alloc] init];
            if ([instance conformsToProtocol:@protocol(Module)]) {
                [self.loadedModules addObject:instance];
                NSLog(@"Module loaded: %@", [instance moduleDescription]);
            }
        } else {
            NSLog(@"Class could not be loaded: %@", moduleClassName);
        }
    }

    [self checkDependencies];
}

- (void) checkDependencies {
    // Create a dictionary of all loaded modules
    NSMutableDictionary *moduleNames = [NSMutableDictionary dictionaryWithCapacity:self.loadedModules.count];
    [self.loadedModules enumerateObjectsUsingBlock:^(id<Module> obj, NSUInteger idx, BOOL *stop) {
        [moduleNames setObject:@YES forKey:[obj moduleDescription]];
    }];

    // Check dependencies
    NSMutableArray *loadedModulesCopy = [self.loadedModules mutableCopy];
    [loadedModulesCopy enumerateObjectsUsingBlock:^(id<Module> obj, NSUInteger idx, BOOL *stop) {
        NSArray *dependencies = [obj moduleDependencies];
        for (NSString *dep in dependencies) {
            id testObject = [moduleNames objectForKey:dep];
            if (!testObject) {
                // Dependency not found
                NSLog(@"WARNING: Dependency '%@' of module '%@' not found. The module will be unloaded.",
                      dep,
                      [obj moduleDescription]);
                [self.loadedModules removeObject:obj];
                break;
            }
        }
    }];

    if (loadedModulesCopy.count != self.loadedModules.count) {
        // unloading a module may force other modules to unload
        // repeat recursively
        [self checkDependencies];
    }
}

- (void)displayModules {
    // reusable values
    float screenWidth = self.moduleScrollView.frame.size.width;
    float screenHeight = self.moduleScrollView.frame.size.height;
    float firstAndLastPadding = screenWidth/2 - MODULE_PREVIEW_WIDTH/2;

    // set scrollable content size
    float contentWidth = self.loadedModules.count * (MODULE_PREVIEW_PADDING + MODULE_PREVIEW_WIDTH) - MODULE_PREVIEW_PADDING + 2 * firstAndLastPadding;
    float contentHeight = MODULE_PREVIEW_WIDTH;
    self.moduleScrollView.contentSize = CGSizeMake(contentWidth, contentHeight);

    // fill content with images
    int i = 0;
    for (id<Module> module in self.loadedModules) {
        UIImage *moduleImage = [module modulePreviewImage];

        UIButton *moduleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        moduleButton.frame = CGRectMake(firstAndLastPadding + i * (MODULE_PREVIEW_WIDTH+MODULE_PREVIEW_PADDING),
                                        screenHeight/2 - MODULE_PREVIEW_WIDTH/2,
                                        MODULE_PREVIEW_WIDTH,
                                        MODULE_PREVIEW_WIDTH);
        moduleButton.layer.masksToBounds = YES;
        moduleButton.layer.cornerRadius = 34.0f;
        moduleButton.backgroundColor = [UIColor darkGrayColor];
        moduleButton.backgroundColor = [moduleButton.backgroundColor colorWithAlphaComponent:0.2];
        moduleButton.tag = i;
        [moduleButton addTarget:self action:@selector(startModule:) forControlEvents:UIControlEventTouchUpInside];
        [moduleButton setImage:moduleImage forState:UIControlStateNormal];

        [self.moduleScrollView addSubview:moduleButton];

        i++;
    }
    
    float startX = self.moduleScrollView.frame.size.width/4 + MODULE_PREVIEW_PADDING/2;
    self.moduleScrollView.contentOffset = CGPointMake(startX, 0);
    
}

#pragma mark - IB Actions

- (IBAction)loadAllData:(id)sender {
    self.loadAllDataButton.enabled = NO;
    self.progressBar.alpha = 1.0;
    self.progressBar.progress = 0.0;

    int numberOfModules = self.loadedModules.count;
    self.moduleLoadingProgress = [NSMutableDictionary dictionaryWithCapacity:numberOfModules];
    self.queuedModules = [NSMutableArray arrayWithCapacity:numberOfModules];

    for (id<Module> module in self.loadedModules) {

        if ([self moduleMayStartLoadingData:module]) {
            DLog(@"Module '%@' will start loading data immediately.", [module description]);
            [self loadModuleData:module];
        } else {
            DLog(@"Module '%@' is queued and its data will be loaded later.", [module description]);
            [self.queuedModules addObject:module];
        }
    }
}

- (void) loadModuleData:(id<Module>)module {
    
    id<CoreDataAppDelegate> appDelegate = (id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    __weak StartScreenController *weakSelf = self;
    UpdateBlock updateBlock = ^(NSNumber* progress, id<Module> sender){
        [weakSelf updateProgress:progress fromModule:sender];
    };
    CompletionBlock completionBlock = ^(NSError *error, id<Module> sender) {
        [weakSelf moduleHasFinishedLoading:error sender:sender];
    };
    
    [module moduleLoadData:context
     usingBlockForProgress:updateBlock
             andCompletion:completionBlock];
}

#pragma mark - Callbacks from Modules

- (void)updateProgress:(NSNumber *)progress fromModule:(id<Module>)sender {
    // DLog(@"Progress: %@ Module: %@", progress, sender);
    [self.moduleLoadingProgress setObject:progress forKey:[sender moduleDescription]];

    float totalProgress = 0;
    int numberOfModules = self.loadedModules.count;
    for (NSNumber *moduleProgress in self.moduleLoadingProgress.allValues) {
        totalProgress += moduleProgress.floatValue/(float)numberOfModules;
    }
    self.progressBar.progress = totalProgress;
}

- (void)moduleHasFinishedLoading:(NSError *)error sender:(id<Module>)sender {
    
    // can more modules start loading?
    NSArray *immutableCopy = [NSArray arrayWithArray:self.queuedModules];
    for (id<Module> module in immutableCopy) {
        if ([self moduleMayStartLoadingData:module]) {
            DLog(@"Module '%@' is now starting to load data.", [module description]);
            [self.queuedModules removeObject:module];
            [self loadModuleData:module];
        } else {
            DLog(@"Module '%@' still waiting.", [module description]);
        }
    }
    
    // have all modules finished yet?
    if (self.progressBar.progress == 1) {
        
        // finished
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationCurveEaseOut
                         animations:^{
                             self.progressBar.alpha = 0.5;
                             self.loadAllDataButton.enabled = YES;
                         }
                         completion:^(BOOL finished) {
                             self.moduleLoadingProgress = nil;
                         }];
    }
}

#pragma mark - Other User Action

- (void)startModule:(id)sender {
    UIButton *senderButton = (UIButton*)sender;
    int tag = senderButton.tag;
    if (tag < self.loadedModules.count) {
        id<Module> selectedModule = [self.loadedModules objectAtIndex:tag];
        UIViewController *baseController = [selectedModule moduleLaunchController];

        // avoid reference to self inside block: possible retain cycle
        __weak UIViewController *weakSelf = self;
        [selectedModule setCloseModuleBlock:^(AnimationHasFinishedBlock block){
            [weakSelf dismissViewControllerAnimated:YES completion:block];
        }];

        // display controller
        baseController.modalPresentationStyle = UIModalPresentationFullScreen;
        baseController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:baseController animated:YES completion:nil];
        NSLog(@"Opening Module: %@", [selectedModule moduleDescription]);
    }
}

#pragma mark - Data Loading Helper methods

- (BOOL)moduleMayStartLoadingData:(id<Module>)module {
    // innocent until proven guilty...
    BOOL allHaveFinished = YES;
    
    NSArray *dependencies = [module moduleDependencies];
    if (!dependencies || dependencies.count == 0) {
        // no dependencies - start loading immediately
        return allHaveFinished;
    } else {
        // maybe the dependencies have finished loading?
        for (NSString *moduleDescription in dependencies) {
            NSNumber *moduleProgress = [self.moduleLoadingProgress objectForKey:moduleDescription];
            if (!moduleProgress || !moduleProgress.floatValue == 1.0) {
                // not finished yet (or not even started)
                allHaveFinished = NO;
            }
        }
    }
    
    return allHaveFinished;
}

@end
