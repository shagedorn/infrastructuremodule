//
//  ServerEntity.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 7/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Designed for objects that are loaded from a server,
 *  typically subclasses of NSManagedObject.
 */
@protocol ServerEntity <NSObject>

/**
 *  Matches dictionary keys and attributes - overwrites all
 *  attributes but the id (or another primary key).
 *
 *  @param infoDict The dictionary that will be searched for keys/values
 *  matching the entity's attributes.
 */
- (void) updateWithInformation:(NSDictionary*)infoDict;

@end
