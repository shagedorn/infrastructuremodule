

/**
 *  Logging in Debug configuration only
 *
 *  Uses the DEBUG flag to check the configuration.
 */

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif