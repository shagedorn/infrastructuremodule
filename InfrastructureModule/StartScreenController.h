//
//  StartScreenController.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  This controller launches all other view controllers.
 *
 *  It does not statically depend on any module. All other
 *  controllers are loaded dynamically. The 'ActiveModules'-PLIST
 *  is used to discover modules.
 */
@interface StartScreenController : UIViewController {}

@end
