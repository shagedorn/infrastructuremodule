//
//  BundleResourceHelperClass.h
//  InfrastructureModule
//
//  Created by Sebastian Hagedorn on 15/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Simplifies access to resources within bundles.
 */
@interface BundleResourceHelperClass : NSObject

/**
 *  Get an absolute path to a resource.
 *
 *  Supports abstraction from retina graphics (@2x naming convention).
 *
 *  @param resName The filename without the extension.
 *  @param ext The file extension.
 *  @param moduleName The name of the module bundle.
 *
 *  @return The absolute path, if found. Else nil.
 */
+ (NSString*)pathForResource:(NSString*)resName
                       ofType:(NSString*)ext
                     inModule:(NSString*)moduleName;

/**
 *  Tries to find a bundle for the given name.
 *
 *  @param moduleName The bundle name.
 *  @return The bundle, if found. Else nil.
 */
+ (NSBundle*) bundleForModule:(NSString*) moduleName;

@end
